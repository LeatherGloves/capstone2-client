
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let token = localStorage.getItem("token")
fetch(`https://afternoon-anchorage-82353.herokuapp.com/api/courses/${courseId}/activate`, {
	method : "PATCH",
	headers : {
		"Authorization" : `Bearer ${token}`
	}
})
.then(res => {return res.json()})
.then(data => {
	if (data) {
		alert("Course Activated")
		window.location.replace('./courses.html');
	} else {
		alert("Something went wrong")
	}
})
