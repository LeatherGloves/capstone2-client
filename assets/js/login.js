let loginForm = document.querySelector("#logInUser")


loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	if (email === "" || password == "") {
		alert("Please input your email and/or password")
	} else {
		fetch("https://afternoon-anchorage-82353.herokuapp.com/api/users/login", {
			method : "POST",
			headers : {
				"Content-Type" : "application/json"
			},
			body : JSON.stringify({
				email : email,
				password : password
			})
		})
		.then(res => res.json())
		.then(data => {
			if (data !== false && data.access !== null) {
				localStorage.setItem("token", data.access)

				fetch("https://afternoon-anchorage-82353.herokuapp.com/api/users/details", {
					headers: {
						Authorization: `Bearer ${data.access}`
					}
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					localStorage.setItem("id", data._id)
					localStorage.setItem("isAdmin", data.isAdmin)
					window.location.replace("./courses.html")

				})
			} else {
				alert("Something went wrong")
			}
		})
	}

})
