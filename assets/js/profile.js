
let token = localStorage.getItem("token")
let userName = document.querySelector("#userName")
let mobileNo = document.querySelector("#mobileNo")
let emailAddress = document.querySelector("#emailAddress")
let enrollContainer = document.querySelector("#enrollContainer")
let adminUser = localStorage.getItem("isAdmin")
let courseNumber;
let cardFooter;

fetch("https://afternoon-anchorage-82353.herokuapp.com/api/users/details", {
	method : "GET",
	headers : {
		"Authorization" : `Bearer ${token}`
		}
	})
.then(res => {return res.json() })
.then(data => {

	userName.innerHTML = `${data.firstname} ${data.lastname}`
	mobileNo.innerHTML = data.mobileNo
	emailAddress.innerHTML = data.email
	courseNumber = data.enrollments

	fetch("https://afternoon-anchorage-82353.herokuapp.com/api/courses")
	.then(res => res.json())
	.then(data => {
		let courseData;

		if (data.length < 1) {
			courseData = "No courses enrolled in yet";
		} else {
			courseData = data.map(course => { //break the array into component elements
				/*console.log(course._id)*/
					cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block"> Go to Course</a>`
						for(enrollments = 0; enrollments < courseNumber.length; enrollments++) {
							if (course._id === courseNumber[enrollments].courseId) {
								return (
									`
										<div class="col-md-6 my-3">
											<div class="card">
												<div class="card-body">
													<h5 class="card-title">${course.name}</h5>
													<p class="card-text text-left">
														${course.description}
													</p>
													<p class="card-text text-right">
														${course.price}
													</p>
												</div>
												<div class="card-footer">
													${cardFooter}
												</div>
											</div>
										</div>
									`
								)
						
							}
					

						}
				}).join ("")
			}
		let container = document.querySelector("#coursesContainer")
		container.innerHTML = courseData
	})
})