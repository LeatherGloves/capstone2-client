let formSubmit = document.querySelector("#createCourse")

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	//get the JWT from localStorage
	let token = localStorage.getItem("token")

	//use fetch to tsend the data for the new course to the backend
	fetch ("https://afternoon-anchorage-82353.herokuapp.com/api/courses", {
		method : "POST",
		headers : {
			"Content-Type" : "application/json",
			//add bearer token as only logged in users can create courses
			"Authorization" : `Bearer ${token}` //This is the bearer token similar to postman
		},
		body : JSON.stringify ({
			name : courseName,
			description : description,
			price : price
		})
	}).then(res => {
		return res.json()
	}).then(data => {
		if(data) {
			window.location.replace("./courses.html")
		} else {
			alert("Course not added")
		}
	})
})