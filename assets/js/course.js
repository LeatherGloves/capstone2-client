//Scenario: when accessing a course, the url is as follows:
// course.html?courseId=5e11645e8124
//in order to get the actual details, we need to get the value after the ?courseId=

//window.location.search returns the query string part of the URL
/*console.log(window.location.search)*/
//?courseId=5ecf99d2dd914d7355fd03a4

//To get the actual courseId, we need to use URL search params to access specific parts of the query
let zuitterNav = document.querySelector("#zuitterNav")
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let adminUser = localStorage.getItem("isAdmin");
let token = localStorage.getItem("token")
let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")

if (localStorage.length === 0) {
	zuitterNav.innerHTML = 
	`
		<ul class="navbar-nav ml-auto">
			<li class="nav-item active">
				<a href="./../index.html" class="nav-link"> Home </a>
			</li>

			<li class="nav-item ">
				<a href="./courses.html" class="nav-link"> Courses </a>
			</li>

			<li class="nav-item ">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>

			<li class="nav-item">
				<a href="./login.html" class="nav-link"> Log in </a>
			</li>
		</ul>
	`
} else if (adminUser == "true"){
	zuitterNav.innerHTML = 
	`
	<ul class="navbar-nav ml-auto">
		<li class="nav-item active">
			<a href="../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>
		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>
	</ul>
	`
} else {
	zuitterNav.innerHTML = 
	`
	<ul class="navbar-nav ml-auto">
		<li class="nav-item active">
			<a href="../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>
		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>
		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
	</ul>
	`
}


//retrieve the JWT stored in localStorage

//populate the information of a course using fetch

if (adminUser == "false" || !adminUser) {
	fetch(`https://afternoon-anchorage-82353.herokuapp.com/api/courses/${courseId}`)
	.then(res => {return res.json() })
	.then(data => {

		courseName.innerHTML = data.name
		courseDesc.innerHTML = data.description
		coursePrice.innerHTML = data.price
		//innerHTML is a property of DOM that specifies the conent inside a HTML tag
		enrollContainer.innerHTML = `<button id="enrollButton" class="btn btn-block btn-primary">Enroll</button>`
		//after the enroll button has been added, we add an event so that when the button is clicked, the student is erolled to a class
		document.querySelector("#enrollButton").addEventListener("click", () => {
			// no need to e.preventDefault, as the button doesn't have any default actions when clicked
			// fetch the enroll function of the backend and send the necessary data to the frontend
			// token and courseId
			// url: http://localhost:3000/api/users/enroll
			//Mini exercise: prepare the fetch statement to the backend
			// for the two thens, just console.log the data
			fetch("https://afternoon-anchorage-82353.herokuapp.com/api/users/enroll", {
				method : "POST",
				headers : {
					"Content-Type" : "application/json",
					"Authorization" : `Bearer ${token}`
				}, 
				body : JSON.stringify ({
					courseId : courseId,
				})
			})
			.then(res => {return res.json() })
			.then(data => {
				if (localStorage.length === 0) {
					alert("Please Log in to enroll")
				} else if (data) {
					alert("You have enrolled successfully")
					window.location.replace("./courses.html")
				} else {
					alert("Enrollment failed")
				}
			})
		})
	})
} else {
	fetch("https://afternoon-anchorage-82353.herokuapp.com/api/users/", {
		method : "GET",
		headers : {
			"Authorization" : `Bearer ${token}`
		}
	})
	.then(res => {return res.json() })
	.then(data => {
		console.log(data)
		const userData = data
		fetch(`https://afternoon-anchorage-82353.herokuapp.com/api/courses/${courseId}`)
		.then(res => {return res.json() })
		.then(data => {
			let enrolleesData;
			var fullList = "";
			courseName.innerHTML = data.name
			courseDesc.innerHTML = data.description
			coursePrice.innerHTML = data.price
			console.log(data.enrollees[0].userId)
			for (let enrolleeCount = 0; enrolleeCount < data.enrollees.length; enrolleeCount++) {
				for (let userLog = 0; userLog < userData.length; userLog++) {
					if (data.enrollees[enrolleeCount].userId === userData[userLog]._id) {
						if (userData[userLog].firstname != undefined && userData[userLog].lastname != undefined ) {
							var enrolleeList = `<li>${userData[userLog].firstname} ${userData[userLog].lastname}</li>`
							fullList = fullList + enrolleeList
							enrolleeContainer.innerHTML = `${fullList}`
						}	
					} 
				}
			}
		})
	})
}