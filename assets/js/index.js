let adminUser = localStorage.getItem("isAdmin");
console.log(adminUser)
if (localStorage.length === 0) {
	zuitterNav.innerHTML = 
	`
		<ul class="navbar-nav ml-auto">
			<li class="nav-item active">
				<a href="./index.html" class="nav-link"> Home </a>
			</li>

			<li class="nav-item ">
				<a href="./pages/courses.html" class="nav-link"> Courses </a>
			</li>

			<li class="nav-item ">
				<a href="./pages/register.html" class="nav-link"> Register </a>
			</li>

			<li class="nav-item">
				<a href="./pages/login.html" class="nav-link"> Log in </a>
			</li>
		</ul>
	`
} else if (adminUser == "true"){
	zuitterNav.innerHTML = 
	`
	<ul class="navbar-nav ml-auto">
		<li class="nav-item active">
			<a href="./index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./pages/courses.html" class="nav-link"> Courses </a>
		</li>
		<li class="nav-item">
			<a href="./pages/logout.html" class="nav-link"> Log Out </a>
		</li>
	</ul>
	`
} else {
	zuitterNav.innerHTML = 
	`
	<ul class="navbar-nav ml-auto">
		<li class="nav-item active">
			<a href="./index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./pages/courses.html" class="nav-link"> Courses </a>
		</li>
		<li class="nav-item">
			<a href="./pages/logout.html" class="nav-link"> Log Out </a>
		</li>
		<li class="nav-item">
			<a href="./pages/profile.html" class="nav-link"> Profile </a>
		</li>
	</ul>
	`
}