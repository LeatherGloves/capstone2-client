let zuitterNav = document.querySelector("#zuitterNav")
let adminUser = localStorage.getItem("isAdmin");
let modalButton = document.querySelector("#adminButton");
let cardFooter;
let token = localStorage.getItem("token")

if (localStorage.length === 0) {
	zuitterNav.innerHTML = 
	`
		<ul class="navbar-nav ml-auto">
			<li class="nav-item active">
				<a href="./../index.html" class="nav-link"> Home </a>
			</li>

			<li class="nav-item ">
				<a href="./courses.html" class="nav-link"> Courses </a>
			</li>

			<li class="nav-item ">
				<a href="./register.html" class="nav-link"> Register </a>
			</li>

			<li class="nav-item">
				<a href="./login.html" class="nav-link"> Log in </a>
			</li>
		</ul>
	`
} else if (adminUser == "true"){
	zuitterNav.innerHTML = 
	`
	<ul class="navbar-nav ml-auto">
		<li class="nav-item active">
			<a href="../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>
		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>
	</ul>
	`
} else {
	zuitterNav.innerHTML = 
	`
	<ul class="navbar-nav ml-auto">
		<li class="nav-item active">
			<a href="../index.html" class="nav-link"> Home </a>
		</li>

		<li class="nav-item ">
			<a href="./courses.html" class="nav-link"> Courses </a>
		</li>
		<li class="nav-item">
			<a href="./logout.html" class="nav-link"> Log Out </a>
		</li>
		<li class="nav-item">
			<a href="./profile.html" class="nav-link"> Profile </a>
		</li>
	</ul>
	`
}
if (adminUser == "false" || !adminUser) {
	fetch("https://afternoon-anchorage-82353.herokuapp.com/api/users/details", {
		method : "GET",
		headers : {
			"Authorization" : `Bearer ${token}`
			}
		})
	.then(res => res.json())
	.then(data =>{
		modalButton.innerHTML = 
		`
			<div class ="col-md-2 offset=md=10">
				<a href="./profile.html?userId=${data._id}" value=${data._id}" class="btn btn-block btn-primary">
					Go to Profile 
				</a>
			</div>
		`
	})
} else {
	modalButton.innerHTML = 
	`
		<div class ="col-md-2 offset=md=10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">
				Add Course 
			</a>
		</div>
	`
}

if(adminUser == "false" || !adminUser) {

	fetch("https://afternoon-anchorage-82353.herokuapp.com/api/courses/active")
	.then(res => res.json())
	.then(data => {
		let courseData;

		if (data.length < 1) {
			courseData = "No courses available";
		} else {
			courseData = data.map(course => {
				cardFooter = `<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block"> Go to Course</a>`
				return (
						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">${course.name}</h5>
										<p class="card-text text-left">
											${course.description}
										</p>
										<p class="card-text text-right">
											${course.price}
										</p>
									</div>
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						`
					)


			}).join ("")
		}
		let container = document.querySelector("#coursesContainer")
		container.innerHTML = courseData
	})
} else {
	fetch("https://afternoon-anchorage-82353.herokuapp.com/api/courses")
	.then(res => res.json())
	.then(data => {
		let courseData;

		if (data.length < 1) {
			courseData = "No courses available";
		} else {
			courseData = data.map(course => {
				if(course.isActive) {
					cardFooter = `<a href="./deleteCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-danger text-white btn-block"> Archive Course</a>
					<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block"> Go to Course</a>`
				} else {
					cardFooter = `<a href="./activateCourse.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block"> Activate Course</a>
					<a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-primary text-white btn-block"> Go to Course</a>`
				}
				return (
						`
							<div class="col-md-6 my-3">
								<div class="card">
									<div class="card-body">
										<h5 class="card-title">${course.name}</h5>
										<p class="card-text text-left">
											${course.description}
										</p>
										<p class="card-text text-right">
											${course.price}
										</p>
									</div>
									<div class="card-footer">
										${cardFooter}
									</div>
								</div>
							</div>
						`
					)
			}).join ("")
		}
		let container = document.querySelector("#coursesContainer")
		container.innerHTML = courseData
	})

}