/*let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")

let token = localStorage.getItem("token")

fetch(`http://localhost:3000/api/courses/${courseId}`, {
	method : "DELETE",
	headers : {
		"Content-Type" : "application/json",
		"Authorization" : `Bearer ${token}`
	}
})
.then(res => {return res.json() })
.then(data => {
	console.log(data)
	if (data) {
		alert("You have archived the course")
		window.location.replace("./courses.html")
	} else {
		alert("Archiving failed")
	}
})*/

//Exercise
//use the lesson for the enroll to archive the course
//Steps
//1. Get the ID from the URL

//To get the actual courseId, we need to use URL search params to acces specific 
let params = new URLSearchParams(window.location.search)
let courseId = params.get("courseId")
let token = localStorage.getItem("token") //cannot delete without admin logged in
//Q How would I know if the request is from an admin
//Validation is done to check if the entries in a form are correct
//Authentication is used to check is user is logged in or not
//Authorization is done to check if the user has privileges for certain actions
//2. Use the ID as a parameter to the fetch and specify the URL
fetch(`https://afternoon-anchorage-82353.herokuapp.com/api/courses/${courseId}`, {
	method : "DELETE",
	headers : {
		"Authorization" : `Bearer ${token}`
	}
})
.then(res => {return res.json()})
.then(data => {
	console.log(data)
	if (data) {
		alert("Course Archived")
		window.location.replace('./courses.html');
	} else {
		alert("Something went wrong")
	}
})
//3. Process the data needed
//4. Return to the courses page